@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Crear nueva categoría</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {!! Form::open(['route' => 'admin.categories.store']) !!}

        <div class="form-group">
            {!! Form::label('name', 'Nombre', ['class' => '']) !!}
            {!! Form::text('name', null, [
                'class' => 'form-control',
                'placeholder' => 'Ingrese el nombre de la categoría'
                ]) !!}

            @error('name')
                <small class="text-danger">{{$message}}</small>
            @enderror
        </div>

        <div class="form-group">
            {!! Form::label('slug', 'Slug', ['class' => '']) !!}
            {!! Form::text('slug', null, [
                'class' => 'form-control',
                'placeholder' => 'Ingrese el slug de la categoría',
                'readonly' => true
                ]) !!}

            @error('slug')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        {!! Form::submit('Crear categoría', ['class' => 'btn btn-sm btn-primary']) !!}

        {!! Form::close() !!}
    </div>
</div>
@stop

@section('js')
    <!-- FIXME -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/speakingurl/14.0.1/speakingurl.min.js" integrity="sha512-i1kgQZJBA3n0k1Ar2++6FKibz8fDlaDpZ8ZLKpCnypYznNL++R6FPxpKJP6NGwsO2sAzzX4x78XjiYrLtMWAfA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{asset('vendor/jQuery-Plugin-stringToSlug-2.1.0/jQuery-Plugin-stringToSlug-2.1.0/src/jquery.stringtoslug.js')}}"></script>

<script>
$(document).ready( function() {
  $("#name").stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: '#slug',
    space: '-'
  });
});
</script>
    
@endsection
