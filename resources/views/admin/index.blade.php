@extends('adminlte::page')
{{-- @section('layouts.app') --}}

{{-- @endsection --}}

@section('title', 'Dashboard')

    {{-- @section('plugins.Sweetalert2', true) --}}

@section('content_header')

    <h1>Atenea</h1>
@stop

@section('content')
    <p>Bienvenido a Atenea.</p>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/custom-adminLTE.css') }}">
@stop

@section('js')
    <script>
        //$('.toast').toast('autohide')
    </script>
@stop
