@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listado de etiquetas</h1>
@stop

@section('content')
    <div id="principal" class="card">
        <div class="card-body">

          <div class="col-xs-8 col-xs-offset-2">
            <div class="input-group">
              <div class="input-group-btn search-panel">
                @php
                    $terms = [
                      "contains" => "Contiene",
                      "its_equal" => "Es igual",
                      "greather_than" => "Mayor que",
                      "less_than" => "Menor que"
                    ]
                @endphp

                <select id="search-by-term" class="form-control input-sm text-info" name="search_term">
                  <option hidden selected>Filtro</option>
                  @foreach ($terms as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                  @endforeach
                </select>
              </div>

              <input type="text" id="text-to-search" class="form-control input-sm" name="text_to_search" placeholder="Buscar...">
              <span class="input-group-btn">
                <button id="btn-search" class="btn btn-sm btn-default form-control" type="button" disabled>
                  <i class="fa fa-search" aria-hidden="true"></i>
                </button>
              </span>
                </div>
            </div>

            <table id="table" class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Color</th>
                  <th style="text-align: right;">
                    <div id="refresh" class="d-inline-block">
                      <a role="button" class="btn btn-sm btn-info ml-2 btn-refresh" aria-label="Nuevo"><i class="fa fa-refresh" aria-hidden="true"></i>
                                <span class="d-none d-md-inline-block ml-1">Refrescar</span>
                      </a>
                    </div>
                    <div id="new-record" class="d-inline-block">
                      <a role="button" class="btn btn-sm btn-info ml-2 btn-create" aria-label="Nuevo"><i class="fas fa-plus"></i>
                        <span class="d-none d-md-inline-block ml-1">Nuevo</span>
                      </a>
                    </div>
                  </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <nav id="pagination" aria-label="Page navigation">
              
            </nav>
          </div>
        </div>
        
    {{-- Modal input --}}
    <div id="input-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            {!! Form::open([
                    'class' => 'needs-validation',
                    'role' => 'form'
                    ]) !!}

              <div class="form-group" id="edit">
                {!! Form::label('modal-id', 'ID') !!}
                {!! Form::text('modal-id', null, [
                  'class' => 'form-control modal-id',
                  'tabindex' => -1,
                  'readonly' => true
                  ]) !!}
              </div>

              <div class="form-group">
                {!! Form::label('modal-name', 'Nombre') !!}
                {!! Form::text('modal-name', null, [
                  'class' => 'form-control modal-name',
                  'placeholder' => 'Nombre',
                  'minlength' => 3,
                  'maxlength' => 255,
                  'required' => true
                  ]) !!}
              </div>

              <div class="form-group">
                {!! Form::label('modal-slug', 'Slug') !!}
                {!! Form::text('modal-slug', null, [
                  'class' => 'form-control modal-slug',
                  'placeholder' => 'slug',
                  'tabindex' => -1,
                  'readonly' => true
                  ]) !!}
              </div>

              <div class="form-group">
                {!! Form::label('modal-color', 'Color') !!}
                {!! Form::select('modal-color', $colors, null, [
                      'class' => 'form-control modal-color',
                      'placeholder' => 'Color',
                      'required' => true
                      ]) !!}
              </div>

              <div class="modal-footer">
                <button type="button"class="btn btn-outline-secondary btn-sm mx-1" aria-label="Cancelar" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="accept-input-modal" class="btn btn-info btn-sm mx-1" aria-label="Guardar">Guardar</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
    
    {{-- Modal delete --}}
    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Alert Modal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
             <h5 class="modal-title alert-modal-title"></h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">E</div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary btn-sm mx-1" aria-label="Cancelar" data-dismiss="modal">Cancelar</button>
            <button id="delete-button" type="buttom" class="btn btn-danger btn-sm mx-1" aria-label="Borrar">Borrar</button>
          </div>
        </div>
      </div>
    </div>      
  </div>
@stop
              
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="{{ asset('css/custom-adminLTE.css') }}">
@stop
              
@section('js')
  <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" defer></script>
  <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js" defer></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/speakingurl/14.0.1/speakingurl.min.js"
    integrity="sha512-i1kgQZJBA3n0k1Ar2++6FKibz8fDlaDpZ8ZLKpCnypYznNL++R6FPxpKJP6NGwsO2sAzzX4x78XjiYrLtMWAfA=="
    crossorigin="anonymous" referrerpolicy="no-referrer">
  </script>
  <script
    src="{{ asset('vendor/jQuery-Plugin-stringToSlug-2.1.0/jQuery-Plugin-stringToSlug-2.1.0/src/jquery.stringtoslug.js') }}">
  </script>
  <script src="{{asset('js/basic-functions.js')}}" defer></script>
  <script src="{{asset('js/tags.js')}}" defer></script>
  <script src="{{asset('js/alert.js') }}" defer></script>
  <script src="{{asset('js/datatables.js')}}" defer></script>

  <script>
    $(() => {
      $("#input-modal .modal-name").stringToSlug({
        setEvents: 'keyup keydown blur',
        getPut: '#input-modal .modal-slug',
        space: '-'
      });
    });
  </script>
@endsection