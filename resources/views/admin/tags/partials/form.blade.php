@if (Request::is('admin/*/edit'))
    <div class="form-group">
        {!! Form::label('id', 'ID') !!}
        {!! Form::text('id', null, ['class' => 'form-control', 'placeholder' => '', 'disabled' => true]) !!}
    </div>
@endif

<div class="form-group">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre de la etiqueta']) !!}

    @error('name')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el slug de la etiqueta', 'tabindex' => -1, 'readonly' => true]) !!}

    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="form-group">
    {!! Form::label('color', 'Color') !!}
    {!! Form::select('color', $colors, null, ['class' => 'form-control', 'placeholder' => 'Eliga el color']) !!}

    @error('color')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
