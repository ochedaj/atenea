@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Mostrar detalle de la etiqueta</h1>
@stop

@section('content')
    <p>Bienvenido a Atenea.</p>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/custom-adminLTE.css') }}">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
