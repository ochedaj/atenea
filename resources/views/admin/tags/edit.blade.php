@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar etiqueta</h1>
@stop

@section('content')
    {{-- @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif --}}

    <div class="card">
        <div class="card-body">
            {!! Form::model($tag, ['route' => ['admin.tags.update', $tag], 'method' => 'put']) !!}

            @include('admin.tags.partials.form')

            {{-- {!! Form::submit('Actualizar etiqueta', ['class' => 'btn btn-sm btn-info']) !!} --}}
            <div class="d-flex justify-content-end">
                <button class="btn btn-info btn-sm mx-1" type="submit" aria-label="Guardar">{{-- <i class="fas fa-check"></i> --}}<span class="ml-1">Guardar</span></button>
                <a role="button" class="btn btn-outline-secondary btn-sm mx-1" href="{{ url()->previous() }}" aria-label="Cancelar">{{-- <i class="fas fa-times"></i> --}}<span class="ml-1">Cancelar</span></a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/custom-adminLTE.css') }}">
@stop

@section('js')
    <!-- FIXME -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/speakingurl/14.0.1/speakingurl.min.js"
        integrity="sha512-i1kgQZJBA3n0k1Ar2++6FKibz8fDlaDpZ8ZLKpCnypYznNL++R6FPxpKJP6NGwsO2sAzzX4x78XjiYrLtMWAfA=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>
    <script
        src="{{ asset('vendor/jQuery-Plugin-stringToSlug-2.1.0/jQuery-Plugin-stringToSlug-2.1.0/src/jquery.stringtoslug.js') }}">
    </script>

    <script>
        $(document).ready(function() {
            $("#name").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
        });

    </script>

@endsection
