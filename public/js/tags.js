/* LIST TAGS*/
class ListTags {
    constructor(url) {
        this._url = url;
    }

    fill(url = this._url) {
        this._url = url;

        fetch(this._url)
            .then((response) => response.json())
            .then((jsonData) => {
                if (jsonData.success == true) {
                    this.colors = jsonData.lists.colors;
                    const tbody = jsonData.paginate.data.reduce((rows, row) => {
                        return (
                            rows +
                            `
                        <tr data-row-id="${row.id}">
                        <td class="col-id">${row.id}</td>
                        <td class="col-name">${row.name}</td>
                        <td class="col-color">${
                            jsonData.lists.colors[row.color]
                        }</td>
                        <td>
                        <div class="d-flex justify-content-end">
                        <button type="button" class="btn btn-outline-success btn-sm btn-row btn-show" data-toggle="modal" data-btn-show-id="${
                            row.id
                        }">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                        <span class="d-none ml-1">Mostrar</span>
                        </button>
                        
                        <button type="button" class="btn btn-outline-info btn-sm btn-row btn-edit" data-toggle="modal" data-btn-edit-id="${
                            row.id
                        }">
                        <i class="fas fa-pen"></i>
                        <span class="d-none ml-1">Editar</span>
                        </button>
                        
                        <button type="button" class="btn btn-outline-danger btn-sm btn-row btn-delete" aria-label="Borrar" data-btn-delete-id="${
                            row.id
                        }">
                        <i class="fas fa-trash-alt"></i>
                        <span class="d-none ml-1">Borrar</span>
                        </button>
                        </div>
                        </td>
                        </tr>
                        `
                        );
                    });

                    this.empty();
                    $("#table tbody").append(tbody);
                    this.pagination(jsonData.paginate);

                    $(".btn-show").on("click", this.show);
                    $(".btn-edit").on("click", FormEdit.show);
                    $(".btn-delete").on("click", FormDelete.show);
                    $("#input-modal").on("hidden.bs.modal", function(e) {
                        const selectors = [
                            "#modal-id",
                            "#modal-name",
                            "#modal-slug",
                            "#modal-color",
                        ];

                        $.each(selectors, function() {
                            $(this).removeAttr("disabled");
                            $("#accept-input-modal").css("display", "inherit");
                        });
                    });
                } else if (jsonData.success == false) {
                    const title = "Error: " + jsonData.error.code;
                    const body = jsonData.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }

    show(e) {
        const id = $(e.currentTarget).data("btnShowId");

        fetch(listTags.url + "/" + id)
            .then((response) => response.json())
            .then((jsonData) => {
                if (jsonData.success == true) {
                    const selectors = [
                        "#modal-id",
                        "#modal-name",
                        "#modal-slug",
                        "#modal-color",
                    ];

                    $.each(selectors, function() {
                        $(this).attr("disabled", true);
                        $("#accept-input-modal").css("display", "none");
                    });

                    $("#input-modal").attr("data-form", "show");
                    $("#input-modal .modal-title").html("Mostrar etiqueta");
                    $("#input-modal .modal-id").val(jsonData.data.id);
                    $("#input-modal .modal-name").val(jsonData.data.name);
                    $("#input-modal .modal-slug").val(jsonData.data.slug);
                    $("#input-modal .modal-color").val(jsonData.data.color);
                    $("#edit").show(); // ocultar/mostar campo ID
                    $("#input-modal").modal("show");
                } else if (jsonData.success == false) {
                    const title = "Error: " + jsonData.error.code;
                    const body = jsonData.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );

                    listTags.removeRow(id);
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }

    empty() {
        $("#principal tbody").empty();
    }

    removeRow(id) {
        //$("tr[data-row-id='" + id + "']").remove();
        //this.fill();
    }

    updateRow(data) {
        $("tr[data-row-id='" + data.id + "']")
            .find(".col-name")
            .html(data.name);
        $("tr[data-row-id='" + data.id + "']")
            .find(".col-color")
            .html(this.colors[data.color]);
    }

    createRow(data) {
        const row = `
        <tr data-row-id="${data.id}">
            <td class="col-id">${data.id}</td>
            <td class="col-name">${data.name}</td>
            <td class="col-color">${this.colors[data.color]}</td>
            <td>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-outline-info btn-sm btn-row btn-edit" data-toggle="modal" data-btn-edit-id="${
                        data.id
                    }">
                        <i class="fas fa-pen"></i>
                        <span class="d-none ml-1">Editar</span>
                    </button>

                    <button type="button" class="btn btn-outline-danger btn-sm btn-row btn-delete" aria-label="Borrar" data-btn-delete-id="${
                        data.id
                    }">
                        <i class="fas fa-trash-alt"></i>
                        <span class="d-none ml-1">Borrar</span>
                    </button>
                </div>
            </td>
        </tr>
        `;

        /* $("#principal tbody").append(row); */

        //$(".btn-show").on("click", listTags.show);
        /* $(".btn-edit").on("click", FormEdit.show);
        $(".btn-delete").on("click", FormDelete.show); */
        this.fill();
    }

    getColors() {
        return this.colors;
    }

    get url() {
        return this._url;
    }

    pagination(paginate) {
        let li = "";
        let a = "";
        let span = "";

        let ul = document.createElement("ul");
        ul.classList.add("pagination", "justify-content-center");

        paginate.links.forEach((element) => {
            li = document.createElement("li");
            li.classList.add("page-item", element.active && "active", !element.url && "disabled");
            a = document.createElement("a");
            a.classList.add("page-link");
            a.setAttribute("href", "javascript:void(0)");

            element.url && a.setAttribute("onclick", `listTags.fill("${element.url}")`);

            a.innerHTML = element.label;
            if (element.active) {
                span = document.createElement("span");
                span.classList.add("sr-only");
                span.innerHTML = "(current)";
                a.appendChild(span);
            }
            li.appendChild(a);
            ul.appendChild(li);
        });

        let nav = document.querySelector("#pagination");
        $("#pagination").empty()
        nav.appendChild(ul);
    }
}

/* CREATE */
class FormCreate {
    static show() {
        $("#input-modal").attr("data-form", "create");
        $("#input-modal .modal-title").html("Nueva etiqueta");
        $("#edit").hide();
        $("#input-modal .modal-name").val(null);
        $("#input-modal .modal-slug").val(null);
        $("#input-modal .modal-color").val(null);
        $("#input-modal").modal("show");
    }

    static store() {
        const data = {
            name: $("#input-modal .modal-name").val(),
            slug: $("#input-modal .modal-slug").val(),
            color: $("#input-modal .modal-color").val(),
        };

        fetch(listTags.url, {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then((res) => res.json())
            .then((response) => {
                if (response.success == false) {
                    const title = "Error : " + response.error.code;
                    const body = response.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );
                } else if (response.success == true) {
                    Alert.show(
                        "fa fa-info-circle",
                        "¡Éxito!",
                        "Etiqueta creada (" + response.data.id + ")",
                        "info"
                    );

                    listTags.createRow(response.data);
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }
}

/* EDIT */
class FormEdit {
    static show(e) {
        const id = $(e.currentTarget).data("btnEditId");

        fetch(listTags.url + "/" + id)
            .then((response) => response.json())
            .then((jsonData) => {
                if (jsonData.success == true) {
                    $("#input-modal").attr("data-form", "edit");
                    $("#input-modal .modal-title").html("Editar etiqueta");
                    $("#input-modal .modal-id").val(jsonData.data.id);
                    $("#input-modal .modal-name").val(jsonData.data.name);
                    $("#input-modal .modal-slug").val(jsonData.data.slug);
                    $("#input-modal .modal-color").val(jsonData.data.color);
                    $("#edit").show();
                    $("#input-modal").modal("show");
                } else if (jsonData.success == false) {
                    const title = "Error: " + jsonData.error.code;
                    const body = jsonData.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );

                    listTags.removeRow(id);
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }

    static store() {
        const data = {
            id: $("#input-modal .modal-id").nval(),
            name: $("#input-modal .modal-name").val(),
            slug: $("#input-modal .modal-slug").val(),
            color: $("#input-modal .modal-color").val(),
        };

        fetch(listTags.url + "/" + data.id, {
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then((res) => res.json())
            .then((response) => {
                if (response.success == false) {
                    const title = "Error : " + response.error.code;
                    const body = response.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );

                    listTags.removeRow(data.id);
                } else if (response.success == true) {
                    Alert.show(
                        "fa fa-info-circle",
                        "¡Éxito!",
                        "Etiqueta actualizada (" + response.data.id + ")",
                        "info"
                    );

                    listTags.updateRow(response.data);
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }
}

/* DELETE */
class FormDelete {
    static show(e) {
        const id = $(e.currentTarget).data("btnDeleteId");

        fetch(listTags.url + "/" + id)
            .then((response) => response.json())
            .then((jsonData) => {
                if (jsonData.success == true) {
                    $("#delete-modal").data("id", jsonData.data.id);
                    $("#delete-modal .modal-title").html("Borrar etiqueta");
                    $("#delete-modal .modal-body").html(jsonData.data.name);
                    $("#delete-modal").modal("show");
                } else if (jsonData.success == false) {
                    const title = "Error: " + jsonData.error.code;
                    const body = jsonData.error.message;

                    Alert.show(
                        "fa fa-exclamation-circle",
                        title,
                        body,
                        "danger"
                    );
                    listTags.removeRow(id);
                }
            })
            .catch(() => {
                Alert.show(
                    "fa fa-exclamation-circle",
                    "Error : 500",
                    "Internal server error",
                    "danger"
                );
            });
    }

    static delete() {
        setTimeout(() => {
            $("#delete-modal").modal("hide");
            const id = $("#delete-modal").data("id");

            fetch(listTags.url + "/" + id, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                    },
                })
                .then((response) => {
                    if (response.status != 204) {
                        const title = "Error : " + response.status;
                        const body = response.statusText;

                        Alert.show(
                            "fa fa-exclamation-circle",
                            title,
                            body,
                            "danger"
                        );
                    } else {
                        Alert.show(
                            "fa fa-info-circle",
                            "¡Éxito!",
                            "Etiqueta borrada (" + id + ")",
                            "info"
                        );

                        listTags.removeRow(id);
                    }
                })
                .catch(() => {
                    Alert.show(
                        "fa fa-exclamation-circle",
                        "Error : 500",
                        "Internal server error",
                        "danger"
                    );
                });
        }, 500);
    }
}

/* CORE */
const listTags = new ListTags("http://atenea.alutek.es/api/tags");
listTags.fill();

$(".btn-refresh").on("click", () => listTags.fill());
$(".btn-create").on("click", FormCreate.show);
$("#delete-button").on("click", FormDelete.delete);

// Fetch all the forms we want to apply custom Bootstrap validation styles to
const forms = $(".needs-validation");
// Loop over them and prevent submission
const validation = Array.prototype.filter.call(forms, (form) => {
    form.addEventListener(
        "submit",
        (event) => {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add("was-validated");
            event.preventDefault();
            setTimeout(() => {
                $("#input-modal").modal("hide");
                form.classList.remove("was-validated");

                switch ($("#input-modal").attr("data-form")) {
                    case "create":
                        FormCreate.store();
                        break;

                    case "edit":
                        FormEdit.store();
                }
            }, 500);
        },
        false
    );
});

// Search

const search = (searchByTerm, textToSearch) => {
    alert(searchByTerm + " ----- " + textToSearch)
}

$('#text-to-search').on('keyup', (e) => {
    const btnSearch = $('#btn-search')

    if (e.currentTarget.value.length >= 3) {
        btnSearch.removeAttr('disabled')

        if (e.keyCode === 13) {
            const searchByTerm = $('#search-by-term').val()
            const textToSearch = $('#text-to-search').val()
            search(searchByTerm, textToSearch)
        }

    } else {
        btnSearch.attr('disabled', true)
    }
})

$('#btn-search').on('click', () => {
    const searchByTerm = $('#search-by-term').val()
    const textToSearch = $('#text-to-search').val()
    search(searchByTerm, textToSearch)
})