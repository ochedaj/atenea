class Alert {
    static show(icon, title, body, colorClass = 'info') {
        setTimeout(() => {
            $(document).Toasts('create', {
                icon,
                title,
                body,
                class: colorClass,
                close: false,
                delay: 3000,
                autohide: true
            })
        }, 300);
    }
}