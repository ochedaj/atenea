<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TagController;

Route::get('', [HomeController::class, 'index'])->name('admin.home');

Route::resource('categories', CategoryController::class)->names('admin.categories');

Route::resource('tags', TagController::class)->names('admin.tags');
/* Route::get('tags', [TagController::class, 'index'])->name('admin.tags.index');
Route::get('tags/{tag:slug}', [TagController::class, 'show'])->name('admin.tags.show');
Route::post('tags', [TagController::class, 'create'])->name('admin.tags.create'); */



// FIXME Borrar...
Route::get('prueba', function() {
  return 'Esta es la página de prueba';
})->name('prueba');

Route::get('ejemplo', function() {
  return 'Esta es la página de ejemplo';
})->name('ejemplo');
