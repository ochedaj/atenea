<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        // Api Errors
        if (Request::is('api/*')) {
            $this->renderable(function (NotFoundHttpException $e, $request) {
                return response()
                    ->json(
                        [
                            'success' => false,
                            'error' => [
                                'code' => 404,
                                'message' => 'Record not found',
                            ]
                        ]
                    )
                    ->setStatusCode(404, 'Not Found');
            });

            $this->renderable(function (Exception $e, $request) {
                return response()
                    ->json(
                        [
                            'success' => false,
                            'error' => [
                                'code' => 500,
                                'message' => 'Internal server error',
                            ]
                        ]
                    )
                    ->setStatusCode(500, 'Internal Server Error');
            });
        }
    }
}
