<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    //protected $perPage = 15;

    protected $fillable = [
        'name',
        'slug',
        'color'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*     public function getRouteKeyName()
    {
        return "slug";
    } */


    public static function colors()
    {
        return [
            'red' => 'Rojo',
            'yellow' => 'Amarillo',
            'green' => 'Verde',
            'blue' => 'Azul',
            'indigo' => 'Indigo',
            'purple' => 'Purple',
            'pink' => 'Rosado'
        ];
    }
}
