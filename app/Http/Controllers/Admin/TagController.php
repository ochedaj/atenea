<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* $tags = Tag::all(); */
        $colors = Tag::colors();

        return view('admin.tags.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colors = Tag::colors();

        return view('admin.tags.create', compact('colors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|unique:tags',
            'color' => 'required'
        ]);

        $tag = Tag::create($request->all());

        $message = [
            //'icon' => 'fas fa-exclamation-triangle',
            'icon' => 'fa fa-info-circle',
            'title' => '¡Éxito!',
            'body' => 'Etiqueta creada'
        ];

        return redirect()->route('admin.tags.index', compact('tag'))->with('alert', json_encode($message));;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        return json_encode($tag);
        //return view('admin.tags.show', compact('tag'))->with('info', 'La etiqueta se creó con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $colors = [
            'red' => 'Color rojo',
            'yellow' => 'Color amarillo',
            'green' => 'Color verde',
            'blue' => 'Color azul',
            'indigo' => 'Color indigo',
            'purple' => 'Color purple',
            'pink' => 'Color rosado'
        ];

        return view('admin.tags.edit', compact('tag', 'colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $request->validate([
            'name' => 'required|max:255',
            'slug' => "required|unique:tags,slug,$tag->id",
            'color' => 'required'
        ]);

        $tag->update($request->all());

        $message = [
            //'icon' => 'fas fa-exclamation-triangle',
            'icon' => 'fa fa-info-circle',
            'title' => '¡Éxito!',
            'body' => 'Etiqueta actualizada'
        ];

        return redirect()->route('admin.tags.index', $tag)->with('alert', json_encode($message));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        $message = [
            //'icon' => 'fas fa-exclamation-triangle',
            'icon' => 'fa fa-info-circle',
            'title' => '¡Éxito!',
            'body' => 'Etiqueta eliminada'
        ];

        return redirect()->route('admin.tags.index')->with('alert', json_encode($message));
    }
}
