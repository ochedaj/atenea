<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()
        ->json([
            'success' => true,
            'paginate' => Tag::paginate()->onEachSide(0),
            'lists' => [
                'colors' => Tag::colors()
            ]
        ])
        ->setStatusCode(200, 'OK');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = Tag::create($request->all());

        return response()
            ->json([
                'success' => true,
                'data' => $tag
            ])
            ->setStatusCode(201, 'OK');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        return response()
            ->json([
                'success' => true,
                'data' => $tag
            ])
            ->setStatusCode(200, 'OK');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $tag->update($request->all());

        return response()
            ->json([
                'success' => true,
                'data' => $tag
            ])
            ->setStatusCode(200, 'OK');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return response()->json(null, 204);
    }
}


/* 
https://www.youtube.com/watch?v=Oj0yF8w0jwE
https://api.jquery.com/jquery.get/
https://www.youtube.com/watch?v=eoGITOPpBfU
https://es.javascript.info/fetch
https://styde.net/api-rest-con-laravel-5-1-proteccion-con-access-key/
https://www.w3adda.com/blog/laravel-5-8-jquery-ajax-form-submit
https://www.ibm.com/docs/es/odm/8.5.1?topic=api-rest-response-codes-error-messages
https://www.toptal.com/laravel/restful-laravel-api-tutorial
*/